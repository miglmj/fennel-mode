# Summary of user-visible changes

## 0.1.0 / 2018-11-01

* Fix imenu support
* Add fennel-view-compilation function
* Make fennel-reload safe for compiler errors
* Fixed a bug where indentation wouldn't work unless lisp-mode was loaded
* Add a setting to disable switching to repl on reload

## 0.0.2 / 2018-05-09

* Add fennel-reload
* Add imenu support
* Add find-definition
* Lots more keywords

## 0.0.1 / 2018-02-18

* Initial commit
